FROM openjdk:11-jre-slim
MAINTAINER luis.flores
ARG JAR_FILE=target/exam-1.0.0.jar
COPY ${JAR_FILE} exam.jar
ENTRYPOINT ["java","-jar","/exam.jar"]