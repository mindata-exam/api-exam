package com.lfr.exam.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "SUPERHEROES")
public class SuperHero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSuperHero")
    private long id;

    @NotNull
    @Size(min = 1, max = 40)
    private String name;
}
