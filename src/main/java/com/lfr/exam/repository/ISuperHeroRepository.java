package com.lfr.exam.repository;

import com.lfr.exam.model.SuperHero;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ISuperHeroRepository extends CrudRepository<SuperHero, Long>{

    Optional<List<SuperHero>> findByNameContaining(String name);

}
