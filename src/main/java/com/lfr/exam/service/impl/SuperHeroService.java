package com.lfr.exam.service.impl;

import com.lfr.exam.exception.NoDataFoundException;
import com.lfr.exam.model.SuperHero;
import com.lfr.exam.repository.ISuperHeroRepository;
import com.lfr.exam.service.ISuperHeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SuperHeroService implements ISuperHeroService {

    private ISuperHeroRepository superHeroRepository;

    @Autowired
    public SuperHeroService(ISuperHeroRepository superHeroRepository) {
        this.superHeroRepository = superHeroRepository;
    }

    @Cacheable(value="superHeroById")
    @Override
    public SuperHero findById(long superHeroId) {
        Optional<SuperHero> superHero = Optional.ofNullable(this.superHeroRepository.findById(superHeroId)
                .orElseThrow(() -> new NoDataFoundException(superHeroId)));

        return superHero.get();
    }

    @Cacheable(value="allSuperHeros")
    @Override
    public List<SuperHero> findAll() {

        List<SuperHero> superHeroes = StreamSupport.stream(this.superHeroRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());

        if (!superHeroes.isEmpty()) {
            return superHeroes;
        } else {
            throw new NoDataFoundException();
        }
    }

    @Cacheable(value="superHeroByName")
    @Override
    public List<SuperHero> findAllByName(String name) {
        Optional<List<SuperHero>> superHeroes = Optional.ofNullable(this.superHeroRepository.findByNameContaining(name)
                .orElseThrow(() -> new NoDataFoundException(name)));

        if (!superHeroes.get().isEmpty()) {
            return superHeroes.get();
        } else {
            throw new NoDataFoundException(name);
        }
    }

    @CacheEvict(value = {"superHeroById", "allSuperHeros", "superHeroByName"}, allEntries=true)
    @Override
    public void updateSuperHero(long superHeroId, SuperHero superHero) {
        Optional<SuperHero> superHeroDB = Optional.ofNullable(this.superHeroRepository.findById(superHeroId)
                .orElseThrow(() -> new NoDataFoundException(superHeroId)));

        superHeroDB.get().setName(superHero.getName().strip());

        this.superHeroRepository.save(superHeroDB.get());
    }

    @CacheEvict(value = {"superHeroById", "allSuperHeros", "superHeroByName"}, allEntries=true)
    @Override
    public void deleteSuperHeroById(long superHeroId) {
        this.superHeroRepository.deleteById(superHeroId);
    }
}
