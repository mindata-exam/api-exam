package com.lfr.exam.service;

import com.lfr.exam.model.SuperHero;

import java.util.List;

public interface ISuperHeroService {

    SuperHero findById(long superHeroId);

    List<SuperHero> findAll();

    List<SuperHero> findAllByName(String name);

    void updateSuperHero(long superHeroId, SuperHero superHero);

    void deleteSuperHeroById(long superHeroId);

}
