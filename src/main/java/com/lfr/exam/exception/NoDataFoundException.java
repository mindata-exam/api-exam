package com.lfr.exam.exception;

public class NoDataFoundException extends RuntimeException {

    public NoDataFoundException(){
        super("Super Heros not found");
    }

    public NoDataFoundException(String superHeroName){
        super(String.format("Super Hero with name '%s' not found", superHeroName));
    }

    public NoDataFoundException(Long idSuperHero){
        super(String.format("Super Hero with id %d not found", idSuperHero));
    }

}
