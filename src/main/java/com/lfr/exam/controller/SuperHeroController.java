package com.lfr.exam.controller;

import com.lfr.exam.annotation.LogTimer;
import com.lfr.exam.model.SuperHero;
import com.lfr.exam.service.ISuperHeroService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/superhero")
public class SuperHeroController {

    private static final Logger LOG = LoggerFactory.getLogger(SuperHeroController.class);

    private ISuperHeroService superHeroService;

    @Autowired
    public SuperHeroController(ISuperHeroService superHeroService) {
        this.superHeroService = superHeroService;
    }

    @LogTimer
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SuperHero>> getAllSuperHeroes(@RequestParam(required = false, value = "superhero-name") Optional<String> superheroName){

        LOG.info("New request getAllSuperHeroes {}", (superheroName.isPresent()) ? superheroName.get().strip() : "");

        List<SuperHero> superHeroes = (superheroName.isPresent()) ?
                this.superHeroService.findAllByName(superheroName.get().strip()) :
                this.superHeroService.findAll();

        return new ResponseEntity<>(superHeroes, HttpStatus.OK);
    }

    @LogTimer
    @GetMapping(value = "/{superhero-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHero> getSuperHero(@PathVariable(value = "superhero-id") long superHeroId){

        LOG.info("New request getSuperHero {}", superHeroId);

        return new ResponseEntity<>(this.superHeroService.findById(superHeroId), HttpStatus.OK);
    }

    @LogTimer
    @PutMapping(value = "/{superhero-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateSuperHero(@PathVariable("superhero-id") long superHeroId,@Valid @RequestBody SuperHero superHero){

        LOG.info("New request updateSuperHero {}", superHeroId);

        this.superHeroService.updateSuperHero(superHeroId, superHero);

        return ResponseEntity.noContent().build();
    }

    @LogTimer
    @DeleteMapping(value = "/{superhero-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteSuperHero(@PathVariable(value = "superhero-id") long superHeroId){

        LOG.info("New request deleteSuperHero {}", superHeroId);

        this.superHeroService.deleteSuperHeroById(superHeroId);

        return ResponseEntity.noContent().build();
    }

}
