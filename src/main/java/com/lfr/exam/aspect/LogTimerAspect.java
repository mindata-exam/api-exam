package com.lfr.exam.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogTimerAspect {

    private static final Logger LOG = LoggerFactory.getLogger(LogTimerAspect.class);

    @Around("@annotation(com.lfr.exam.annotation.LogTimer)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {

        final long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        final long executionTime = System.currentTimeMillis() - start;

        LOG.info("{} executed in {} ms", joinPoint.getSignature(), executionTime);

        return proceed;
    }

}
