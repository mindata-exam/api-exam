package com.lfr.exam.controller;

import com.lfr.exam.model.SuperHero;
import com.lfr.exam.service.ISuperHeroService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SuperHeroController.class)
public class SuperHeroControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ISuperHeroService superHeroService;

    @Test
    public void findAll() throws Exception {
        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("Batman");

        Mockito.when(superHeroService.findAll()).thenReturn(Arrays.asList(batman));

        mockMvc.perform(MockMvcRequestBuilders.get("/superhero")
                .with(SecurityMockMvcRequestPostProcessors.user("usr").password("pass")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Batman")));
    }

    @Test
    public void findAllByName() throws Exception {
        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("Batman");

        SuperHero ironman = new SuperHero();
        ironman.setId(2);
        ironman.setName("Iron Man");

        Mockito.when(superHeroService.findAllByName("man")).thenReturn(Arrays.asList(batman, ironman));

        mockMvc.perform(MockMvcRequestBuilders.get("/superhero").queryParam("superhero-name","man")
                .with(SecurityMockMvcRequestPostProcessors.user("usr").password("pass")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Batman")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.is(2)));
    }

    @Test
    public void findAllById() throws Exception {

        SuperHero ironman = new SuperHero();
        ironman.setId(2);
        ironman.setName("Iron Man");

        Mockito.when(superHeroService.findById(1)).thenReturn(ironman);

        mockMvc.perform(MockMvcRequestBuilders.get("/superhero/1")
                .with(SecurityMockMvcRequestPostProcessors.user("usr").password("pass")))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.aMapWithSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Iron Man")));
    }

    @Test
    public void updateSuperHero() throws Exception {

        SuperHero ironman = new SuperHero();
        ironman.setId(1);
        ironman.setName("Iron Man");

        String bodyRequest = "{\"name\": \"ironman\"}";

        Mockito.doNothing().when(superHeroService).updateSuperHero(1, ironman);

        mockMvc.perform(MockMvcRequestBuilders.put("/superhero/1")
                .with(SecurityMockMvcRequestPostProcessors.user("usr").password("pass"))
                .content(bodyRequest)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void delete() throws Exception {

        Mockito.doNothing().when(superHeroService).deleteSuperHeroById(1);

        mockMvc.perform(MockMvcRequestBuilders.delete("/superhero/1")
                .with(SecurityMockMvcRequestPostProcessors.user("usr").password("pass"))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void authFailure() throws Exception {
        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("batman");

        Mockito.when(superHeroService.findAll()).thenReturn(Arrays.asList(batman));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/superhero"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

}
