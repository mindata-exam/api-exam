package com.lfr.exam.repository;

import com.lfr.exam.model.SuperHero;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class SuperHeroRepositoryTests {

    @Autowired
    private ISuperHeroRepository superHeroRepository;

    @Test
    public void findAll() {

        List<SuperHero> superHeroes = StreamSupport.stream(this.superHeroRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());

        Assert.isTrue(superHeroes.size() == 6 , "More superheroes than expected");
    }

    @Test
    public void findByNameContaining() {
        String superHeroName = "man";
        Optional<List<SuperHero>> superHeroes = this.superHeroRepository.findByNameContaining(superHeroName);
        Assert.isTrue(superHeroes.get().size() == 3 , "More superheroes than expected");

    }

    @Test
    public void findById() {

        long superHeroId = 1;

        Optional<SuperHero> superHero = this.superHeroRepository.findById(superHeroId);

        Assert.isTrue(superHero.isPresent(), "No superhero found");
        Assert.isTrue(superHero.get().getName().equals("Batman"), "Superhero wrong");

    }

    @Test
    public void findEmpty() {

        List<SuperHero> superHeroes = new ArrayList<>();
        Optional<List<SuperHero>> superHeroesOptional;
        String superHeroName = "bat";
        Optional<SuperHero> superHero;
        long superHeroId = 1;

        this.superHeroRepository.deleteAll();

        this.superHeroRepository.findAll().forEach(superHeroes::add);
        Assert.isTrue(superHeroes.isEmpty(), "More superheroes than expected");

        superHero = this.superHeroRepository.findById(superHeroId);
        Assert.isTrue(!superHero.isPresent(), "More superheroes than expected");

        superHeroesOptional = this.superHeroRepository.findByNameContaining(superHeroName);
        Assert.isTrue(superHeroesOptional.get().isEmpty(), "More superheroes than expected");
    }

}