package com.lfr.exam.integration;

import com.lfr.exam.controller.SuperHeroController;
import com.lfr.exam.model.SuperHero;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class IntegrationTests {

    @Autowired
    private SuperHeroController superHeroController;

    @Test
    public void findAll(){

        ResponseEntity<List<SuperHero>> response = superHeroController.getAllSuperHeroes(Optional.ofNullable(null));

        Assert.isTrue(response.getStatusCode().value() == HttpStatus.OK.value(), "Invalid http code");
        Assert.isTrue(!response.getBody().isEmpty(), "Wrong data");
        Assert.isTrue(response.getBody().size() == 6, "Wrong data");

    }

    @Test
    public void findAllByName(){

        String name = "man";

        ResponseEntity<List<SuperHero>> response = superHeroController.getAllSuperHeroes(Optional.of(name));
        Assert.isTrue(response.getStatusCode().value() == HttpStatus.OK.value(), "Invalid http code");
        Assert.isTrue(!response.getBody().isEmpty(), "Wrong data");
        Assert.isTrue(response.getBody().size() == 3, "Wrong data");

    }

    @Test
    public void findAllById(){

        long superHeroId = 1;

        ResponseEntity<SuperHero> response = superHeroController.getSuperHero(superHeroId);
        Assert.isTrue(response.getStatusCode().value() == HttpStatus.OK.value(), "Invalid http code");
        Assert.notNull(response.getBody(), "Wrong data");
        Assert.isTrue(response.getBody().getName().equals("Batman"), "Wrong data");

    }

    @Test
    public void update(){

        long superHeroId = 1;

        SuperHero batman = new SuperHero();
        batman.setName("Goku");

        ResponseEntity<Object> response = superHeroController.updateSuperHero(superHeroId, batman);
        Assert.isTrue(response.getStatusCode().value() == HttpStatus.NO_CONTENT.value(), "Invalid http code");

        ResponseEntity<SuperHero> responseFind = superHeroController.getSuperHero(superHeroId);
        Assert.isTrue(responseFind.getStatusCode().value() == HttpStatus.OK.value(), "Invalid http code");
        Assert.isTrue(responseFind.getBody().getName().equals("Goku"), "Invalid http code");


    }

    @Test
    public void delete(){

        long superHeroId = 1;

        ResponseEntity<Object> response = superHeroController.deleteSuperHero(superHeroId);
        Assert.isTrue(response.getStatusCode().value() == HttpStatus.NO_CONTENT.value(), "Invalid http code");

        ResponseEntity<List<SuperHero>> responseFindAll = superHeroController.getAllSuperHeroes(Optional.ofNullable(null));
        Assert.isTrue(responseFindAll.getStatusCode().value() == HttpStatus.OK.value(), "Invalid http code");
        Assert.isTrue(!responseFindAll.getBody().isEmpty(), "Wrong data");
        Assert.isTrue(responseFindAll.getBody().size() == 5, "Wrong data");

        SuperHero batman = responseFindAll.getBody()
                .stream()
                .filter(superHero -> superHero.getId() == superHeroId)
                .findAny()
                .orElse(null);

        Assert.isNull(batman, "Elemento no eliminado");

    }

}
