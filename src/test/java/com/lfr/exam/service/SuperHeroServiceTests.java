package com.lfr.exam.service;

import com.lfr.exam.exception.NoDataFoundException;
import com.lfr.exam.model.SuperHero;
import com.lfr.exam.repository.ISuperHeroRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class SuperHeroServiceTests {

    @MockBean
    ISuperHeroRepository superHeroRepository;

    @Autowired
    ISuperHeroService superHeroService;

    @Test
    public void findAllEmpty() {
        Mockito.when(superHeroRepository.findAll()).thenReturn(new ArrayList<>());

        NoDataFoundException exception = Assertions.assertThrows(NoDataFoundException.class, () -> {
            superHeroService.findAll();
        });

        Assertions.assertEquals("Super Heros not found", exception.getMessage());
        Mockito.verify(superHeroRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void findAll() {

        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("batman");

        SuperHero ironman = new SuperHero();
        ironman.setId(2);
        ironman.setName("ironman");

        Mockito.when(superHeroRepository.findAll()).thenReturn(Arrays.asList(batman, ironman));
        Assert.isTrue(superHeroService.findAll().size() == 2 , "More superheroes than expected");
        Mockito.verify(superHeroRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void findByNameContainingEmpty() {

        String superHeroName = "bat";

        Mockito.when(superHeroRepository.findByNameContaining(superHeroName)).thenReturn(Optional.ofNullable(null));

        NoDataFoundException exception = Assertions.assertThrows(NoDataFoundException.class, () -> {
            superHeroService.findAllByName(superHeroName);
        });

        Assertions.assertEquals("Super Hero with name 'bat' not found", exception.getMessage());
        Mockito.verify(superHeroRepository, Mockito.times(1)).findByNameContaining(superHeroName);
    }

    @Test
    public void findByNameContaining() {

        String superHeroName = "bat";

        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("batman");

        Mockito.when(superHeroRepository.findByNameContaining(superHeroName)).thenReturn(Optional.of(Arrays.asList(batman)));
        Assert.isTrue(superHeroService.findAllByName(superHeroName).size() == 1 , "More superheroes than expected");
        Mockito.verify(superHeroRepository, Mockito.times(1)).findByNameContaining(superHeroName);
    }

    @Test
    public void findByIdEmpty() {

        long superHeroId = 1;

        Mockito.when(superHeroRepository.findById(superHeroId)).thenReturn(Optional.ofNullable(null));

        NoDataFoundException exception = Assertions.assertThrows(NoDataFoundException.class, () -> {
            superHeroService.findById(superHeroId);
        });

        Assertions.assertEquals("Super Hero with id 1 not found", exception.getMessage());
        Mockito.verify(superHeroRepository, Mockito.times(1)).findById(superHeroId);
    }

    @Test
    public void findById() {

        long superHeroId = 1;

        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("batman");

        Mockito.when(superHeroRepository.findById(superHeroId)).thenReturn(Optional.of(batman));
        Assert.notNull(superHeroService.findById(superHeroId), "More superheroes than expected");
        Mockito.verify(superHeroRepository, Mockito.times(1)).findById(superHeroId);
    }

    @Test
    public void updateNotFound() {

        long superHeroId = 1;

        SuperHero batman = new SuperHero();
        batman.setId(1);
        batman.setName("batman");

        Mockito.when(superHeroRepository.findById(superHeroId)).thenReturn(Optional.ofNullable(null));

        NoDataFoundException exception = Assertions.assertThrows(NoDataFoundException.class, () -> {
            superHeroService.updateSuperHero(superHeroId,batman);
        });

        Assertions.assertEquals("Super Hero with id 1 not found", exception.getMessage());
        Mockito.verify(superHeroRepository, Mockito.times(1)).findById(superHeroId);
    }

}
