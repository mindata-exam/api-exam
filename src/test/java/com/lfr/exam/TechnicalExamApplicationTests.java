package com.lfr.exam;

import com.lfr.exam.controller.SuperHeroController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class TechnicalExamApplicationTests {

	@Autowired
	private SuperHeroController superHeroController;

	@Test
	void contextLoads() {
		Assert.notNull(superHeroController, "Null controller");
	}

}
