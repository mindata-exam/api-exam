# Instalación
1. El equipo donde ejecutara el proyecto debe tener instalado la versión 11 de Java para poder compilar el API.
2. El equipo donde ejecutara el proyecto debe tener instalado Docker.
3. En el directorio raiz del proyecto (donde se encuentra el archivo pom.xml), ejecutar las siguientes instrucciones desde cmd, para generar la compilación y dockerización del API
 ```sh
    mvnw clean install
```
Se generará la imagen de Docker llamada exam:1.0.0

4.- Ejecutar para desplegar el servicio 
```sh
    mvnw spring-boot:run
```
5.- Para ejecutar crear el contenedor de Docker ejecutar la siguiente instrucción
```sh
    docker run -p 8080:8080 exam:1.0.0
```
La documentación del Api se encontrara en la siguiente url http://localhost:8080/api/v1/documentation/api-docs

Asi mismo cuenta con una UI de Swagger configurada en la url para facilitar las llamadas a la API http://localhost:8080/api/v1/swagger-ui/

El proyecto cuenta con una DB h2 que almacena en memoria los datos solicitados en el requerimiento y esto podra ser consultado desde la consola de administracion de h2 desplegado en la url http://localhost:8080/api/v1/h2-console

Los datos solicitados para ingresar a la consola son los siguientes:
    1. JDBC URL -> jdbc:h2:mem:exam
    2. User Name -> sa
    3. Password -> sa

Para validar los datos persistidos ejecutar la siguiente consulta
- SELECT * FROM SUPERHEROES 
 
Se utilizo Liquibase para la gestion de scripts de DB, los archivos de configuración se encuentran en resources/db.
 
**Nota: La API cuenta con auntenticación basica, por lo que solicitara usuario y password para consumir los endpoints** 

**USER : usr**
**PASSWORD : pass**